#ifndef TOKEN_H_
#define TOKEN_H_

#include <string>

namespace token
{

enum TokenType
{
    // Single-character tokens
    LEFT_PAREN, RIGHT_PAREN, LEFT_BRACE, RIGHT_BRACE, COMMA, DOT, MINUS, PLUS, SEMICOLON, SLASH, STAR,

    // One or two character tokens
    BANG, BANG_EQUAL, EQUAL, EQUAL_EQUAL, GREATER, GREATER_EQUAL, LESS, LESS_EQUAL,

    // Literals
    IDENTIFIER, STRING, NUMBER,

    // Keywords
    AND, CLASS, ELSE, FALSE, FUN, FOR, IF, NIL, OR, PRINT, RETURN, SUPER, THIS, TRUE, VAR, WHILE,

    FIN
};

static const char* TokenTypeStrings[] = {"LEFT_PARENT", "RIGHT_PAREN", "LEFT_BRACE", "RIGHT_BRACE", "COMMA", "DOT", "MINUS", "PLUS", "SEMICOLON", "SLASH", "STAR", "BANG", "BANG_EQUAL", "EQUAL", "EQUAL_EQUAL", "GREATER", "GREATER_EQUAL", "LESS", "LESS_EQUAL", "IDENTIFIER", "STRING", "NUMBER", "AND", "CLASS", "ELSE", "FALSE", "FUN", "FOR", "IF", "NIL", "OR", "PRINT", "RETURN", "SUPER", "THIS", "TRUE", "VAR", "WHILE", "FIN"};

// https://stackoverflow.com/questions/16210707/c-vector-of-template-class

class Token
{
public:
    std::string toString() const;
private:
};

template <typename Literal>
class TokenImpl
{
public:
    TokenImpl(const TokenType&, const std::string&, const Literal, const int)
    {
        this->type = type;
        this->lexeme = lexeme;
        this->literal = literal;
        this->line = line;
    }

    std::string toString() const
    {
        return std::string(getStringTokenType(type) + " " + lexeme + " " + literal);
    }

    const TokenType type;
    const std::string lexeme;
    const Literal literal;
    const int line;

private:
    const char* getStringTokenType(const int token)
    {
        return TokenTypeStrings[token];
    }
};

} // namespace token

#endif
