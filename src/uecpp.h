#ifndef UECPP_H
#define UECPP_H

#include <string>

namespace uecpp
{

class Uecpp
{
public:
    Uecpp();

    static void runFile(const std::string&);
    static void runPrompt();

private:
    static void run(const std::string&);

    static void error(const int, const std::string&);
    static void report(const int, const std::string&, const std::string&);

    static bool hadError;
};

} // namespace uecpp
#endif
