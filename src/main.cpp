#include <cstdlib>
#include <iostream>
#include <memory>

#include "uecpp.h"

int main(int argc, char* argv[])
{
    if (argc > 2)
    {
        std::cout << "Usage: uecpp [script]";
        std::exit(64);
    }
    else if (argc == 2)
    {
        std::unique_ptr<uecpp::Uecpp> uecpp = std::make_unique<uecpp::Uecpp>();
        uecpp->runFile(argv[1]);
    }
    else
    {
        std::unique_ptr<uecpp::Uecpp> uecpp = std::make_unique<uecpp::Uecpp>();
        uecpp->runPrompt();
    }

    return 0;
}
