#include "scanner.h"

#include <memory>
#include <vector>

namespace scanner
{

Scanner::Scanner(const std::string& source)
    : start{0}
    , current{0}
    , line{1}
{
    this->source = source;
}

std::vector<std::unique_ptr<token::Token>> Scanner::scanTokens()
{
    while(!isAtEnd())
    {
        start = current;
        //scanToken();
    }

    tokens.push_back(std::make_unique<token::Token>(EOF, "", 0, line));
    return tokens;
}

bool Scanner::isAtEnd() const
{
    return current >= source.length();
}

} // namespace scanner
