#include "scanner.h"
#include "token.h"
#include "uecpp.h"
#include "utils.h"

#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

namespace uecpp
{

Uecpp::Uecpp() { hadError = false; }

void Uecpp::runFile(const std::string& path)
{
    std::vector<char> bytes = utils::readAllBytes(path);
    run(std::string(bytes.begin(), bytes.end()));

    if (hadError)
    {
        std::exit(65);
    }
}

void Uecpp::runPrompt()
{
    for (;;)
    {
        std::cout << "> ";
        std::string command;
        std::getline(std::cin, command);
        run(command);
        hadError = false;
    }
}

void Uecpp::run(const std::string& source)
{
    std::unique_ptr<scanner::Scanner> scanner = std::make_unique<scanner::Scanner>(source);
    std::vector<token::Token*> tokens = scanner->scanTokens();

    for (const auto& token : tokens)
    {
        std::cout << token->toString() << "\n";
    }
}

void Uecpp::error(const int line, const std::string& message)
{
    report(line, "", message);
}

void Uecpp::report(const int line, const std::string& where, const std::string& message)
{
    std::cerr << "[line " << line << "] Error" << where << ": " << message;
    hadError = true;
}

} // namespace uecpp
