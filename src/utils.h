#ifndef HELPERS_H
#define HELPERS_H

#include <fstream>
#include <string>
#include <vector>

namespace utils
{

static std::vector<char> readAllBytes(const std::string& filename)
{
    std::ifstream ifs(filename, std::ios::binary|std::ios::ate);
    std::ifstream::pos_type pos = ifs.tellg();

    std::vector<char> result(pos);

    ifs.seekg(0, std::ios::beg);
    ifs.read(result.data(), pos);

    return result;
}

} // namespace utils

#endif
