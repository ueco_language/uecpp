#ifndef SCANNER_H_
#define SCANNER_H_

#include "token.h"

#include <memory>
#include <string>
#include <vector>

namespace scanner
{

class Scanner
{
public:
    Scanner(const std::string&);

    std::vector<std::unique_ptr<token::Token>> scanTokens();

private:
    bool isAtEnd() const;

    std::string source;
    std::vector<std::unique_ptr<token::Token>> tokens;
    int start;
    int current;
    int line;
};

} // namespace scanner

#endif
